#Finding Prototypes of Answers for Improving Answer Sentence Selection

This repository is created for storing scripts and data used with one of the baseline systems mentioned in Tam et al.: Finding Prototypes of Answers for Improving Answer Sentence Selection, IJCAI 2017. This baseline systems on which data transformed by our technique are trained and evaluated is Rao et al.: Noisy-Contrastive Estimation for Answer Selection with Deep Neural Networks, CIKM 2016. 

The implementation of Rao et al. 2016 can be found in [https://github.com/castorini/NCE-CNN-Torch](https://github.com/castorini/NCE-CNN-Torch). We make almost no change to the codes taken from Rao's repository. The major addition we make to the content of Rao's repository are as follows:

* add to the folder `scripts` the script `dataConvert.py`, which does the replacement of wh-words with prototypes of answers. 

* add to the folder `data` the transformed and untransformed data used in our experiment
    * `TrecQAOld` and `WikiQAOld` contains untransformed TrecQA and WikiQA data respectively. 
    * `TrecQA` and `WikiQA` contains transformed TrecQA and WikiQA data respectively.